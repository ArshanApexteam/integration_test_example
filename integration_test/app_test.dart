import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:integration_test_example/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('end-to-end test', () {
    testWidgets('tap next primary number, verify primary number',
        (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      List<int> givenPrimaryNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23];
      final Finder nextPrimaryNumber =
          find.byKey(const Key('Next primary number'));

      for (int i = 0; i < givenPrimaryNumbers.length; i++) {
        expect(find.text(givenPrimaryNumbers[i].toString()), findsOneWidget);
        if (i != givenPrimaryNumbers.length) {
          await tester.tap(nextPrimaryNumber);
          await tester.pumpAndSettle();
        }
      }
    });
  });
}
