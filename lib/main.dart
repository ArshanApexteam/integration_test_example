import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 1;
  int _primaryNumber = 2;

  bool _checkPrime(int number) {
    int i, m = 0, flag = 0;
    m = number ~/ 2;

    for (i = 2; i <= m; i++) {
      if (number % i == 0) {
        flag = 1;
        break;
      }
    }

    if (flag == 0) {
      return true;
    }
    return false;
  }

  void _nextPrimaryNumber() {
    setState(() {
      _counter++;
      int number = _primaryNumber;

      while (true) {
        number++;
        if (_checkPrime(number)) {
          _primaryNumber = number;
          break;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Primary number $_counter:',
            ),
            Text(
              '$_primaryNumber',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _nextPrimaryNumber,
        tooltip: 'Next primary number',
        child: const Icon(Icons.add),
        key: const Key('Next primary number'),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
